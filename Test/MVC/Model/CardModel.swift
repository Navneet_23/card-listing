//
//  CardModel.swift
//  Test
//
//  Created by Navneet  on 26/05/21.
//

import Foundation

struct CardList: Codable {
    var links: WelcomeLinks?
    var timestamp, operation, resultCode, resultInfo: String?
    var returnCode: ReturnCode?
    var status, interaction: Interaction?
    var identification: Identification?
    var networks: Networks?
    var operationType: OperationType?
    var style: Style?
    var payment: Payment?
    var integrationType: String?
}


// MARK: - Identification
struct Identification: Codable {
    var longID, shortID, transactionID: String?

    enum CodingKeys: String, CodingKey {
        case longID = "longId"
        case shortID = "shortId"
        case transactionID = "transactionId"
    }
}

// MARK: - Interaction
struct Interaction: Codable {
    var code, reason: String?
}

// MARK: - WelcomeLinks
struct WelcomeLinks: Codable {
    var linksSelf: String?
    var lang: String?

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case lang
    }
}

// MARK: - Networks
struct Networks: Codable {
    var applicable: [Applicable]?
}

// MARK: - Applicable
struct Applicable: Codable {
    var code, label, method, grouping: String?
    var registration, recurrence: Recurrence?
    var redirect: Bool?
    var links: ApplicableLinks?
    var selected: Bool?
    var inputElements: [InputElement]?
    var operationType: OperationType?
    var contractData: ContractData?
}

// MARK: - ContractData
struct ContractData: Codable {
    var pageEnvironment, javascriptIntegration, pageButtonLocale: String?

    enum CodingKeys: String, CodingKey {
        case pageEnvironment = "PAGE_ENVIRONMENT"
        case javascriptIntegration = "JAVASCRIPT_INTEGRATION"
        case pageButtonLocale = "PAGE_BUTTON_LOCALE"
    }
}

// MARK: - InputElement
struct InputElement: Codable {
    var name: Name?
    var type: TypeEnum?
}

// Name.swift

import Foundation

enum Name: String, Codable {
    case bic = "bic"
    case expiryMonth = "expiryMonth"
    case expiryYear = "expiryYear"
    case holderName = "holderName"
    case iban = "iban"
    case number = "number"
    case verificationCode = "verificationCode"
}

// TypeEnum.swift

import Foundation

enum TypeEnum: String, Codable {
    case integer = "integer"
    case numeric = "numeric"
    case string = "string"
}

// MARK: - ApplicableLinks
struct ApplicableLinks: Codable {
    var logo: String?
    var linksSelf: String?
    var lang: String?
    var operation, validation: String?

    enum CodingKeys: String, CodingKey {
        case logo
        case linksSelf = "self"
        case lang, operation, validation
    }
}

// OperationType.swift

import Foundation

enum OperationType: String, Codable {
    case charge = "CHARGE"
}

// Recurrence.swift

import Foundation

enum Recurrence: String, Codable {
    case none = "NONE"
    case recurrenceOPTIONAL = "OPTIONAL"
}

// MARK: - Payment
struct Payment: Codable {
    var reference: String?
    var amount: Int?
    var currency: String?
}

// MARK: - ReturnCode
struct ReturnCode: Codable {
    var name, source: String?
}

// MARK: - Style
struct Style: Codable {
    var language: String?
}
