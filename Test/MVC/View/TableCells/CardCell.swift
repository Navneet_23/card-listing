//
//  CardCell.swift
//  Test
//
//  Created by Navneet  on 26/05/21.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(data : Applicable){
        self.lblName.text = data.label
        let url = URL(string: data.links?.logo ?? "")
        DispatchQueue.global(qos: .background).async {
            let img = LoadImage.getImageFromURL(imgUrl: (url ?? URL(string: ""))!)
            DispatchQueue.main.async {
                self.imgLogo.image = img
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
