//
//  ViewController.swift
//  Test_Payoneer
//
//  Created by Navneet  on 19/05/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblCards: UITableView!
    
    var cardList = CardList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callDelegates()
    }

    //MARK:- table functions
    func callDelegates(){
        self.tblCards.delegate = self
        self.tblCards.dataSource = self
        self.callCardList()
    }

}

//MARK:- Webservice
extension ViewController{
    func callCardList(){
        WebService.service(API.cardList, param: [:], service: .get, showHud: true){ (model:CardList, data, json) in
            self.cardList = model
            self.tblCards.reloadData()
        }
    }
    
}

//MARK:- Table Delegate/Datasource
extension ViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardList.networks?.applicable?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCell
        let data = self.cardList.networks?.applicable?[indexPath.row]
        cell.setData(data: data ?? Applicable())
        return cell
    }
}
