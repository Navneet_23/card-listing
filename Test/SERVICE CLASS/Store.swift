
import Foundation

class Store {
    
    class var authKey: String?
    {
        set{
            Store.saveValue(newValue, .authKey)
        }get{
            return Store.getValue(.authKey) as? String
        }
    }
    
    class var userid: String?
       {
           set{
               Store.saveValue(newValue, .userid)
           }get{
               return Store.getValue(.userid) as? String
           }
       }
    
    class var LoginType: String?
          {
              set{
                  Store.saveValue(newValue, .LoginType)
              }get{
                  return Store.getValue(.LoginType) as? String
              }
          }
    
    class var NotificationStatus: Int?
             {
                 set{
                     Store.saveValue(newValue, .notificationstatus)
                 }get{
                     return Store.getValue(.notificationstatus) as? Int
                 }
             }
    
   
    
    
    class var deviceToken: String?
    {
        set{
            Store.saveValue(newValue, .deviceToken)
        }get{
            return Store.getValue(.deviceToken) as? String
        }
    }
    
//    class var userDetails: LoginResponseModel?
//    {
//        set{
//            Store.saveUserDetails(newValue, .userDetails)
//            Store.authKey = newValue?.body.authKey
//            Store.userid = "\(newValue?.body.id ?? 0)"
//            Store.NotificationStatus = newValue?.body.notificationStatus
//
//
//        }get{
//            return Store.getUserDetails(.userDetails)
//        }
//    }
    
    class var autoLogin: Bool
    {
        set{
            Store.saveValue(newValue, .autoLogin)
        }
        get{
            return Store.getValue(.autoLogin) as? Bool ?? false
        }
    }
    
    static var remove: DefaultKeys!{
        didSet{
            Store.removeKey(remove)
        }
    }
    
    //MARK:-  Private Functions
    private class func removeKey(_ key: DefaultKeys)
    {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        if key == .userDetails{
            UserDefaults.standard.removeObject(forKey: DefaultKeys.authKey.rawValue)
        }
        UserDefaults.standard.synchronize()
    }
    
    private class func saveValue(_ value: Any? ,_ key:DefaultKeys)
    {
       
        var data: Data?
        if let value = value
        {
          //  data = NSKeyedArchiver.archivedData(withRootObject: value)
            data = try? NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: true)
        }
        UserDefaults.standard.set(data, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    private class func saveUserDetails<T: Codable>(_ value: T?, _ key: DefaultKeys)
    {
        var data: Data?
        if let value = value
        {
            data = try? PropertyListEncoder().encode(value)
        }
        Store.saveValue(data, key)
    }
    
    private class func getUserDetails<T: Codable>(_ key: DefaultKeys) -> T?{
        if let data = self.getValue(key) as? Data{
            let loginModel = try? PropertyListDecoder().decode(T.self, from: data)
            return loginModel
        }
        return nil
    }
    
    private class func getValue(_ key: DefaultKeys) -> Any
    {
        if let data = UserDefaults.standard.value(forKey: key.rawValue) as? Data{
            if let value = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
            {
                return value ?? ""
            }
            else{
                return ""
            }
        }else{
            return ""
        }
    }
}
