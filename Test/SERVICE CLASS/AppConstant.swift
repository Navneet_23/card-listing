
import Foundation
import UIKit


var appName = ""
var noInternetConnection = "No Internet Connection Available"

let deviceType = "2"

let defaultColor1 = "427080"
let defaultColor2 = "384c71"
let defaultColor3 = "f7e1c8"
let defaultColor4 = "d4d6d9"
let defaultColor5 = "697f96"
let defaultColor6 = "cdebfd"

enum DefaultKeys: String
{
    case authKey
    case email
    case password
    case userDetails
    case autoLogin
    case deviceToken
    case userid
    case LoginType
    case notificationstatus
}

enum Services: String
{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

// MARK:- dateconvert
func dateConverter(_ TimeStamp:String,type:String) -> String
{
let unixTimestamp = Double(TimeStamp)
    let date = Date(timeIntervalSince1970: unixTimestamp ?? 0)
    let dateFormatter = DateFormatter()
    //dateFormatter.locale = NSLocale.current
    //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
  
    dateFormatter.dateFormat = type
    let strDate = dateFormatter.string(from: date)
  //  let dateTimeStamp  = strDate.timeIntervalSince1970
    return strDate
    
 
    
}
func convertTimestampToDate(timestamp : String,type : String)-> String{
    let date = Date(timeIntervalSince1970: Double(timestamp)!)
    let dateFormatter = DateFormatter()
    dateFormatter.locale = NSLocale.current
    //            MM/dd/yyy
    dateFormatter.dateFormat = type
    let strDate = dateFormatter.string(from: date)
    return strDate
    
}



enum API:String
{
    case cardList = "https://raw.githubusercontent.com/optile/checkout-android/develop/shared-test/lists/listresult.json"

    
}

func ArrayOfDictionaryToJsonString(_ DataArray:[[String:Any]]) -> String
{
    let jsonData = try? JSONSerialization.data(withJSONObject: DataArray, options: [])
    let sendingStr = String(data: jsonData!, encoding: .utf8)!
    return sendingStr
}

enum Alert: String
{
    case alert = "Alert"
    case error = "Error"
    case name = "Ethnic Wear"
    case image = "Please select image."
}
var mainStoryboard:UIStoryboard{
    return UIStoryboard(name: "Main", bundle: Bundle.main)
}
let kAccesToken = "accessToken"
let kuserType = "userType"
